# Joystick Controlled DC Motor



## Component List


| Component | Quantity |
| ------ | ------ |
| Arduino Uno R3 | 1 |
| DC Motor | 1 |
| 9V Battery | 1 |
| Breadboard | 1 |
| Breadboard Power Supply | 1 |
| L293D H-bridge Motor Driver | 1 |
| Joystick Module | 1 |


## Circuit Schematic
[Can be found here](https://gitlab.com/arduino82/joystick-controlled-dc-motor/-/blob/c3eaea21ae9bb8c09383b7afd0ae2424c17ba8f0/Circuit_Schematic.png)


## Code
[Can be found here](https://gitlab.com/arduino82/joystick-controlled-dc-motor/-/blob/c3eaea21ae9bb8c09383b7afd0ae2424c17ba8f0/joystick-controlled-dc-motor.ino)
