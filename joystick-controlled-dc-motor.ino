// Motor configuration
const int MOTOR_POWER_PIN = 5;
const int DIRECTION_A_PIN = 3;
const int DIRECTION_B_PIN = 4;

// Joystick configuration
const int JOYSTICK_X_PIN = 0;
const int DIRECTION_A_MINIMUM_VALUE = 500;
const int DIRECTION_A_MAXIMUM_VALUE = 0;
const int DIRECTION_B_MINIMUM_VALUE = 510;
const int DIRECTION_B_MAXIMUM_VALUE = 1023;

// Output configuration
const int MINIMUM_OUTPUT_VALUE = 0;
const int MAXIMUM_OUTPUT_VALUE = 255;

void setup() {
  pinMode(MOTOR_POWER_PIN, OUTPUT);
  pinMode(DIRECTION_A_PIN, OUTPUT);
  pinMode(DIRECTION_B_PIN, OUTPUT);
  pinMode(JOYSTICK_X_PIN, INPUT);
  Serial.begin(9600);
}

void loop() {
  int motorOutputPower;
  const int joystickXValue = analogRead(JOYSTICK_X_PIN);
  Serial.println(joystickXValue);

  if (joystickXValue < DIRECTION_A_MINIMUM_VALUE) {
    digitalWrite(DIRECTION_A_PIN, HIGH);
    digitalWrite(DIRECTION_B_PIN, LOW);
    motorOutputPower = map(joystickXValue, DIRECTION_A_MINIMUM_VALUE, DIRECTION_A_MAXIMUM_VALUE, MINIMUM_OUTPUT_VALUE, MAXIMUM_OUTPUT_VALUE); 
  } else if (joystickXValue > DIRECTION_B_MINIMUM_VALUE) {
    digitalWrite(DIRECTION_A_PIN, LOW);
    digitalWrite(DIRECTION_B_PIN, HIGH);
    motorOutputPower = map(joystickXValue, DIRECTION_B_MINIMUM_VALUE, DIRECTION_B_MAXIMUM_VALUE, MINIMUM_OUTPUT_VALUE, MAXIMUM_OUTPUT_VALUE); 
  } else {
    digitalWrite(DIRECTION_A_PIN, LOW);
    digitalWrite(DIRECTION_B_PIN, LOW); 
  }
  analogWrite(MOTOR_POWER_PIN, motorOutputPower);
}
